package com.absence.web.commands;

import com.cgi.interview.service.AbsenceLocalService;
import com.liferay.counter.kernel.service.CounterLocalService;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import absence.web.constants.AbsenceWebPortletKeys;


@Component(
	immediate = true,
	property = {
		"javax.portlet.name="+AbsenceWebPortletKeys.ABSENCEWEB,
		"mvc.command.name=/absence/delete"
	},
	service = MVCActionCommand.class
)
public class DeleteEntryActionCommand extends BaseMVCActionCommand {

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {

		long  absenceId = ParamUtil.getLong(actionRequest, "absenceId");

		absenceLocalService.deleteAbsence(absenceId);

		
	}
	
	
	@Reference
	private CounterLocalService counterLocalService;
	
	@Reference
	private AbsenceLocalService absenceLocalService;

}
