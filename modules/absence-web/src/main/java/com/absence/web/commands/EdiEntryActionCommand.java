package com.absence.web.commands;

import com.cgi.interview.model.Absence;
import com.cgi.interview.service.AbsenceLocalService;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.concurrent.TimeUnit;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import absence.web.constants.AbsenceWebPortletKeys;


@Component(
	immediate = true,
	property = {
		"javax.portlet.name="+AbsenceWebPortletKeys.ABSENCEWEB,
		"mvc.command.name=/absence/edit"
	},
	service = MVCActionCommand.class
)
public class EdiEntryActionCommand extends BaseMVCActionCommand {

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		ThemeDisplay themeDisplay = (ThemeDisplay)actionRequest.getAttribute(
                WebKeys.THEME_DISPLAY);
		User loggedinUser = themeDisplay.getUser();
		
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		
		long absenceId = ParamUtil.getLong(actionRequest, "absenceId");
		
		
		Absence absence = absenceLocalService.getAbsence(absenceId);

		
		String userFullName = loggedinUser.getFullName();
		
		long diff = absence.getStartDate().getTime() - absence.getEndDate().getTime();
		
		actionRequest.setAttribute("userFullName", userFullName);
		actionRequest.setAttribute("absence", absence);
		actionRequest.setAttribute("startDate", df.format(absence.getStartDate()));
		actionRequest.setAttribute("endDate", df.format(absence.getEndDate()));
	
	}
	
	@Reference
	private AbsenceLocalService absenceLocalService;

}
