package com.absence.web.commands;

import com.cgi.interview.model.Absence;
import com.cgi.interview.service.AbsenceLocalService;
import com.liferay.counter.kernel.service.CounterLocalService;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import absence.web.constants.AbsenceWebPortletKeys;


@Component(
	immediate = true,
	property = {
		"javax.portlet.name="+AbsenceWebPortletKeys.ABSENCEWEB,
		"mvc.command.name=/absence/add"
	},
	service = MVCActionCommand.class
)
public class AddEntryActionCommand extends BaseMVCActionCommand {

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		ThemeDisplay themeDisplay = (ThemeDisplay)actionRequest.getAttribute(
                WebKeys.THEME_DISPLAY);
		User loggedinUser = themeDisplay.getUser();
		
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		
		long userId = loggedinUser.getUserId();
		String type = ParamUtil.getString(actionRequest, "absenceType");
		Date startDate = ParamUtil.getDate(actionRequest, "startDate", df);
		Date endDate = ParamUtil.getDate(actionRequest, "endDate", df);
		String reason = ParamUtil.getString(actionRequest, "reason");
		
		System.out.println("reason: " + reason);
		
		
		long absenceId = counterLocalService.increment();
		Absence absence = absenceLocalService.createAbsence(absenceId);
		
		absence.setUserId(userId);
		absence.setType(type);
		absence.setStartDate(startDate);
		absence.setEndDate(endDate);
		absence.setReason(reason);
		
		absenceLocalService.addAbsence(absence);
	
		
		
	}
	
	
	@Reference
	private CounterLocalService counterLocalService;
	
	@Reference
	private AbsenceLocalService absenceLocalService;

}
