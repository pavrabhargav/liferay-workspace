package absence.web.portlet;

import com.cgi.interview.model.Absence;
import com.cgi.interview.service.AbsenceLocalService;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;

import java.io.IOException;
import java.util.List;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import absence.web.constants.AbsenceWebPortletKeys;

/**
 * @author bhargav.pavra
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=AbsenceWeb",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + AbsenceWebPortletKeys.ABSENCEWEB,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class AbsenceWebPortlet extends MVCPortlet {
	
	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		
		ThemeDisplay themeDisplay = (ThemeDisplay)renderRequest.getAttribute(
                WebKeys.THEME_DISPLAY);
		
		User loggedinUser = themeDisplay.getUser();
		String userFullName = loggedinUser.getFullName();
		
		renderRequest.setAttribute("userFullName", userFullName);
		
		List<Absence> allAbsences = absenceLocalService.getAbsences(-1, -1);
		
		renderRequest.setAttribute("allAbsences", allAbsences);
		
		super.doView(renderRequest, renderResponse);
		
	}
	
	@Reference
	private AbsenceLocalService absenceLocalService;
}