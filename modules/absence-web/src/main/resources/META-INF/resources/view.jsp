<%@ include file="/init.jsp" %>


<portlet:actionURL var="addAbsenceURL" name="/absence/add">
</portlet:actionURL>
<portlet:actionURL var="editAbsenceURL" name="/absence/edit">
</portlet:actionURL>


<div class="list-view-icon-wrapper">
	<i class="fa fa-list-ul" aria-hidden="true"></i>
</div>

<aui:form action="<%= addAbsenceURL %>" name="<portlet:namespace />fm">

	<h2>Absence Form</h2>

<%-- <aui:model-context bean="<%= entry %>" model="<%= GuestbookEntry.class %>" /> --%>

	<div class="row">
		<div class="col-md-6">
			<aui:input name="employeeName" value="${userFullName}" disabled="<%=true %>">
				 <aui:validator name="required" />
			</aui:input>
		</div>
		<div class="col-md-6">
			<aui:select name="absenceType">
			    <aui:option value="AA0" selected="${absence.type == 'AA0'}">Sick Leave</aui:option>
			    <aui:option value="AA1" selected="${absence.type == 'AA1'}">Partial Day</aui:option>
			    <aui:validator name="required" />
			</aui:select>
		</div>
	</div>

	<div class="row">
		<div class="col-md-6">
			<div class="form-group input-text-wrapper">
				<label class="control-label" 
					for="<portlet:namespace/>startDate">
							Start Date
						<span class="reference-mark text-warning" id="qfkd__column1__0"><svg class="lexicon-icon lexicon-icon-asterisk" focusable="false" role="presentation" viewBox="0 0 512 512">
							<path class="lexicon-icon-outline" d="M323.6,190l146.7-48.8L512,263.9l-149.2,47.6l93.6,125.2l-104.9,76.3l-96.1-126.4l-93.6,126.4L56.9,435.3l92.3-123.9L0,263.8l40.4-122.6L188.4,190v-159h135.3L323.6,190L323.6,190z"></path>
							</svg></span>
						<span class="hide-accessible">Required</span>
				</label>
				<input class="field form-control" type="text" id="startDate" name="<portlet:namespace/>startDate" value="${startDate}">
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group input-text-wrapper">
				<label class="control-label" 
					for="<portlet:namespace/>endDate">
							End Date
						<span class="reference-mark text-warning" id="qfkd__column1__0"><svg class="lexicon-icon lexicon-icon-asterisk" focusable="false" role="presentation" viewBox="0 0 512 512">
							<path class="lexicon-icon-outline" d="M323.6,190l146.7-48.8L512,263.9l-149.2,47.6l93.6,125.2l-104.9,76.3l-96.1-126.4l-93.6,126.4L56.9,435.3l92.3-123.9L0,263.8l40.4-122.6L188.4,190v-159h135.3L323.6,190L323.6,190z"></path>
							</svg></span>
						<span class="hide-accessible">Required</span>
				</label>
				<input class="field form-control" type="text" id="endDate" name="<portlet:namespace/>endDate" value="${endDate}">
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-6">
			<aui:input name="numberOfDays" disabled="<%=true %>" value="0">
				 <aui:validator name="required" />
			</aui:input>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<aui:input name="reason" type="textarea" value="${absence.reason}">
			</aui:input>
		</div>
	</div>
	
	<aui:button-row>

		<aui:button type="submit" value="Submit"></aui:button>

	</aui:button-row>

	
</aui:form>

<div id="accordion">

<c:forEach items="${allAbsences}" var="absences">

	<portlet:actionURL var="deleteAbsenceURL" name="/absence/delete">
		<portlet:param name="absenceId" value="${absences.absenceId}" />
	</portlet:actionURL>
	
	<portlet:actionURL var="editAbsenceURL" name="/absence/edit">
		<portlet:param name="absenceId" value="${absences.absenceId}" />
	</portlet:actionURL>
 
   
    <h3>
    	<div class="row">
    		<div class="col-md-2"></div>
    		<div class="col-md-8">${absences.type} From ${absences.startDate} to ${absences.endDate} </div>
    	</div>
     </h3>
	  <div>
	  	<div class="row">
	  		<div class="col-md-2"></div>
	  		<div class="col-md-8">
	  			<p><strong>Start Date:</strong>${absences.startDate} &nbsp; <strong>End Date:</strong>${absences.endDate} </p>
	   			<p><strong>Description:</strong>${absences.reason}</p>
	  		</div>
	  		<div class="col-md-2 action-icon-wrapper">
	  			<a href="<%=editAbsenceURL %>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
	  			<a href="<%=deleteAbsenceURL %>"><i class="fa fa-trash" aria-hidden="true"></i></a>
	  		</div>
	  	</div>
	  </div>
</c:forEach>
 

</div>

 <script>
  $( function() {
    $( "#accordion" ).accordion();
  } );
  
  $( function() {
	  
	var numberOfDays = 0;
	var endDate = $('#endDate').val();
	var startDate = $('#startDate').val();
	const oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
	
	  
    $( "#startDate" ).datepicker({
    	onSelect: function(dateText) {
			
    		startDate = toDate($('#startDate').val());
    		
    		numberOfDays= 0;
    		
    		if ($('#endDate').val() != "" ) {
        		endDate = toDate($('#endDate').val());
        		if (startDate == endDate) {
        			numberOfDays = 1;
        		}
        		else {
        			numberOfDays = Math.round(Math.abs((endDate - startDate) / oneDay));
        		}
        		    			
    		}

    		$('#<portlet:namespace/>numberOfDays').val(numberOfDays);

        }
    });
    $( "#endDate" ).datepicker({
    	onSelect: function(dateText) {
    		endDate = toDate($('#endDate').val());
    		numberOfDays= 0;
    		
    		if ($('#startDate').val() != "" ) {
        		
        		startDate = toDate($('#startDate').val());
        		
        		if (startDate == endDate) {
        			numberOfDays = 1;
        		}
        		else {
        			numberOfDays = Math.round(Math.abs((endDate - startDate) / oneDay));
        		}

    		}

    		
    		$('#<portlet:namespace/>numberOfDays').val(numberOfDays);
        }
    });
  } );
  
  function toDate(dateStr) {
	  var parts = dateStr.split("/")
	  return new Date(parts[2], parts[0] - 1, parts[1])
	}
  </script>