package com.cgi.interview.absence.rest.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserAbsenceVO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4653827523150911751L;

	private List<AbsenceVO> userAbsence;

	public List<AbsenceVO> getUserAbsence() {
		return userAbsence;
	}

	public void setUserAbsence(List<AbsenceVO> userAbsence) {
		this.userAbsence = userAbsence;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((userAbsence == null) ? 0 : userAbsence.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserAbsenceVO other = (UserAbsenceVO) obj;
		if (userAbsence == null) {
			if (other.userAbsence != null)
				return false;
		} else if (!userAbsence.equals(other.userAbsence))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "UserAbsenceVO [userAbsence=" + userAbsence + "]";
	}
	
	

}
