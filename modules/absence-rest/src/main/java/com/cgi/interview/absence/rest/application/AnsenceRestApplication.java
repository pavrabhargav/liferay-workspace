package com.cgi.interview.absence.rest.application;


import com.cgi.interview.absence.rest.model.AbsenceVO;
import com.cgi.interview.absence.rest.model.UserAbsenceVO;
import com.cgi.interview.model.Absence;
import com.cgi.interview.service.AbsenceLocalService;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.liferay.counter.kernel.service.CounterLocalService;
import com.liferay.portal.kernel.bean.BeanPropertiesUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.security.permission.PermissionThreadLocal;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.workflow.WorkflowConstants;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants;

/**
 * @author Pranav
 */
@Component(
	property = {
		JaxrsWhiteboardConstants.JAX_RS_APPLICATION_BASE + "=/absence",
		JaxrsWhiteboardConstants.JAX_RS_NAME + "=Greetings.Rest"
	},
	service = Application.class
)
public class AnsenceRestApplication extends Application {

	public Set<Object> getSingletons() {
		Set<Object> singletons = new HashSet<Object>();
		singletons.add(this);
        //add the automated Jackson marshaller for JSON.
        singletons.add(new JacksonJsonProvider());

        return singletons;
	}
	
	@GET
	@Path("/username/{userId}")
	@Produces({MediaType.TEXT_PLAIN})
	public String getUserName(@PathParam("userId") long userId) throws PortalException {
		return userLocalService.getUser(userId).getFullName();
	}
	
	@GET
	@Path("/{absenceId}")
	@Produces({MediaType.APPLICATION_JSON})
	public AbsenceVO getAbsenceById(@PathParam("absenceId") long absenceId) throws PortalException {
		
		System.out.println("getAbsenceById userId: ");

		Absence absence = absenceLocalService.getAbsence(absenceId);
		AbsenceVO absenceVO = new AbsenceVO();
		BeanPropertiesUtil.copyProperties(absence, absenceVO);
		return absenceVO;
	}

	@GET
	@Path("/user/{userId}")
	@Produces({MediaType.APPLICATION_JSON})
	public UserAbsenceVO getUserAbsence(@PathParam("userId") long userId) {
		
		System.out.println("getUserAbsence userId: " + userId);

		UserAbsenceVO userAbsenceVO = new UserAbsenceVO();
		
		List<Absence> absenceList = new ArrayList<Absence>();
		List<AbsenceVO> absenceVOList = new ArrayList<AbsenceVO>();
		
		try {
			absenceList = absenceLocalService.findByuserId(userId);
			
			for (Absence absence: absenceList) {
				AbsenceVO absenceVO = new AbsenceVO();
				BeanPropertiesUtil.copyProperties(absence, absenceVO);
				absenceVOList.add(absenceVO);
			}
			
			userAbsenceVO.setUserAbsence(absenceVOList);

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return userAbsenceVO;
	}
	
	@POST
	@Path("/")
	@Produces({ MediaType.APPLICATION_JSON })
	public String addAbsence(AbsenceVO absenceVO, @Context HttpServletRequest httpServletRequest) throws PortalException {
		
		if (absenceVO.getAbsenceId() != 0L) {
			Absence absence = absenceLocalService.getAbsence(absenceVO.getAbsenceId());
			BeanPropertiesUtil.copyProperties(absenceVO, absence);
			absence = absenceLocalService.updateAbsence(absence);
			
			return "updated";
		} else {
			PermissionChecker pc = PermissionThreadLocal.getPermissionChecker();
			ServiceContext sc = ServiceContextFactory.getInstance(Absence.class.getName(), httpServletRequest);
			sc.setScopeGroupId(pc.getUser().getGroupId());
			sc.setCompanyId(pc.getCompanyId());
			sc.setWorkflowAction(WorkflowConstants.ACTION_PUBLISH);
			
			try {

				DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
				Date startDate = df.parse(absenceVO.getStartDate());
				Date endDate = df.parse(absenceVO.getEndDate());

				Absence absence = absenceLocalService.addAbsence(sc, absenceVO.getUserId(), absenceVO.getType(), startDate, endDate, absenceVO.getReason());
				BeanPropertiesUtil.copyProperties(absence, absenceVO);

			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return "Added";
		}
		
		
	}
	
	@PUT
	@Path("/")
	@Produces({ MediaType.APPLICATION_JSON })
	public String updateAbsence(AbsenceVO absenceVO, @Context HttpServletRequest httpServletRequest) throws PortalException {
		
		PermissionChecker pc = PermissionThreadLocal.getPermissionChecker();
		ServiceContext sc = ServiceContextFactory.getInstance(Absence.class.getName(), httpServletRequest);
		sc.setScopeGroupId(pc.getUser().getGroupId());
		sc.setCompanyId(pc.getCompanyId());
		sc.setWorkflowAction(WorkflowConstants.ACTION_PUBLISH);

		try {

			DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			Date startDate = df.parse(absenceVO.getStartDate());
			Date endDate = df.parse(absenceVO.getEndDate());
			
			Absence absence = absenceLocalService.getAbsence(absenceVO.getAbsenceId());
			
			absence.setType(absenceVO.getType());
			absence.setReason(absenceVO.getReason());
			absence.setStartDate(startDate);
			absence.setEndDate(endDate);

			absence = absenceLocalService.updateAbsence(absence);
			BeanPropertiesUtil.copyProperties(absence, absenceVO);

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "Updated";
	}


	
	  @DELETE
	  @Path("/{absenceId}")
	  @Produces({MediaType.APPLICATION_JSON})
	  public String morning(@PathParam("absenceId") long absenceId) {

		try {
			absenceLocalService.deleteAbsence(absenceId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	  }

	
	@Reference(unbind="-")
	private CounterLocalService counterLocalService;
	
	@Reference(unbind="-")
	private UserLocalService userLocalService;
	
	@Reference(unbind="-")
	private AbsenceLocalService absenceLocalService;

}