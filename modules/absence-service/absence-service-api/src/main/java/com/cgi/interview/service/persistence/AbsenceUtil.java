/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.cgi.interview.service.persistence;

import com.cgi.interview.model.Absence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * The persistence utility for the absence service. This utility wraps <code>com.cgi.interview.service.persistence.impl.AbsencePersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AbsencePersistence
 * @generated
 */
public class AbsenceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Absence absence) {
		getPersistence().clearCache(absence);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, Absence> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Absence> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Absence> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Absence> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Absence> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Absence update(Absence absence) {
		return getPersistence().update(absence);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Absence update(
		Absence absence, ServiceContext serviceContext) {

		return getPersistence().update(absence, serviceContext);
	}

	/**
	 * Returns all the absences where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching absences
	 */
	public static List<Absence> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the absences where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AbsenceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of absences
	 * @param end the upper bound of the range of absences (not inclusive)
	 * @return the range of matching absences
	 */
	public static List<Absence> findByUuid(String uuid, int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the absences where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AbsenceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of absences
	 * @param end the upper bound of the range of absences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching absences
	 */
	public static List<Absence> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Absence> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the absences where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AbsenceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of absences
	 * @param end the upper bound of the range of absences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching absences
	 */
	public static List<Absence> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Absence> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first absence in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching absence
	 * @throws NoSuchAbsenceException if a matching absence could not be found
	 */
	public static Absence findByUuid_First(
			String uuid, OrderByComparator<Absence> orderByComparator)
		throws com.cgi.interview.exception.NoSuchAbsenceException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first absence in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching absence, or <code>null</code> if a matching absence could not be found
	 */
	public static Absence fetchByUuid_First(
		String uuid, OrderByComparator<Absence> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last absence in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching absence
	 * @throws NoSuchAbsenceException if a matching absence could not be found
	 */
	public static Absence findByUuid_Last(
			String uuid, OrderByComparator<Absence> orderByComparator)
		throws com.cgi.interview.exception.NoSuchAbsenceException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last absence in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching absence, or <code>null</code> if a matching absence could not be found
	 */
	public static Absence fetchByUuid_Last(
		String uuid, OrderByComparator<Absence> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the absences before and after the current absence in the ordered set where uuid = &#63;.
	 *
	 * @param absenceId the primary key of the current absence
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next absence
	 * @throws NoSuchAbsenceException if a absence with the primary key could not be found
	 */
	public static Absence[] findByUuid_PrevAndNext(
			long absenceId, String uuid,
			OrderByComparator<Absence> orderByComparator)
		throws com.cgi.interview.exception.NoSuchAbsenceException {

		return getPersistence().findByUuid_PrevAndNext(
			absenceId, uuid, orderByComparator);
	}

	/**
	 * Removes all the absences where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of absences where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching absences
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the absence where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchAbsenceException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching absence
	 * @throws NoSuchAbsenceException if a matching absence could not be found
	 */
	public static Absence findByUUID_G(String uuid, long groupId)
		throws com.cgi.interview.exception.NoSuchAbsenceException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the absence where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching absence, or <code>null</code> if a matching absence could not be found
	 */
	public static Absence fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the absence where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching absence, or <code>null</code> if a matching absence could not be found
	 */
	public static Absence fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, useFinderCache);
	}

	/**
	 * Removes the absence where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the absence that was removed
	 */
	public static Absence removeByUUID_G(String uuid, long groupId)
		throws com.cgi.interview.exception.NoSuchAbsenceException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of absences where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching absences
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the absences where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching absences
	 */
	public static List<Absence> findByUuid_C(String uuid, long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the absences where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AbsenceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of absences
	 * @param end the upper bound of the range of absences (not inclusive)
	 * @return the range of matching absences
	 */
	public static List<Absence> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the absences where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AbsenceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of absences
	 * @param end the upper bound of the range of absences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching absences
	 */
	public static List<Absence> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Absence> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the absences where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AbsenceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of absences
	 * @param end the upper bound of the range of absences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching absences
	 */
	public static List<Absence> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Absence> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first absence in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching absence
	 * @throws NoSuchAbsenceException if a matching absence could not be found
	 */
	public static Absence findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<Absence> orderByComparator)
		throws com.cgi.interview.exception.NoSuchAbsenceException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first absence in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching absence, or <code>null</code> if a matching absence could not be found
	 */
	public static Absence fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<Absence> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last absence in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching absence
	 * @throws NoSuchAbsenceException if a matching absence could not be found
	 */
	public static Absence findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<Absence> orderByComparator)
		throws com.cgi.interview.exception.NoSuchAbsenceException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last absence in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching absence, or <code>null</code> if a matching absence could not be found
	 */
	public static Absence fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<Absence> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the absences before and after the current absence in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param absenceId the primary key of the current absence
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next absence
	 * @throws NoSuchAbsenceException if a absence with the primary key could not be found
	 */
	public static Absence[] findByUuid_C_PrevAndNext(
			long absenceId, String uuid, long companyId,
			OrderByComparator<Absence> orderByComparator)
		throws com.cgi.interview.exception.NoSuchAbsenceException {

		return getPersistence().findByUuid_C_PrevAndNext(
			absenceId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the absences where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of absences where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching absences
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns all the absences where groupId = &#63; and status = &#63;.
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @return the matching absences
	 */
	public static List<Absence> findBystatus(long groupId, int status) {
		return getPersistence().findBystatus(groupId, status);
	}

	/**
	 * Returns a range of all the absences where groupId = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AbsenceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @param start the lower bound of the range of absences
	 * @param end the upper bound of the range of absences (not inclusive)
	 * @return the range of matching absences
	 */
	public static List<Absence> findBystatus(
		long groupId, int status, int start, int end) {

		return getPersistence().findBystatus(groupId, status, start, end);
	}

	/**
	 * Returns an ordered range of all the absences where groupId = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AbsenceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @param start the lower bound of the range of absences
	 * @param end the upper bound of the range of absences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching absences
	 */
	public static List<Absence> findBystatus(
		long groupId, int status, int start, int end,
		OrderByComparator<Absence> orderByComparator) {

		return getPersistence().findBystatus(
			groupId, status, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the absences where groupId = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AbsenceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @param start the lower bound of the range of absences
	 * @param end the upper bound of the range of absences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching absences
	 */
	public static List<Absence> findBystatus(
		long groupId, int status, int start, int end,
		OrderByComparator<Absence> orderByComparator, boolean useFinderCache) {

		return getPersistence().findBystatus(
			groupId, status, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first absence in the ordered set where groupId = &#63; and status = &#63;.
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching absence
	 * @throws NoSuchAbsenceException if a matching absence could not be found
	 */
	public static Absence findBystatus_First(
			long groupId, int status,
			OrderByComparator<Absence> orderByComparator)
		throws com.cgi.interview.exception.NoSuchAbsenceException {

		return getPersistence().findBystatus_First(
			groupId, status, orderByComparator);
	}

	/**
	 * Returns the first absence in the ordered set where groupId = &#63; and status = &#63;.
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching absence, or <code>null</code> if a matching absence could not be found
	 */
	public static Absence fetchBystatus_First(
		long groupId, int status,
		OrderByComparator<Absence> orderByComparator) {

		return getPersistence().fetchBystatus_First(
			groupId, status, orderByComparator);
	}

	/**
	 * Returns the last absence in the ordered set where groupId = &#63; and status = &#63;.
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching absence
	 * @throws NoSuchAbsenceException if a matching absence could not be found
	 */
	public static Absence findBystatus_Last(
			long groupId, int status,
			OrderByComparator<Absence> orderByComparator)
		throws com.cgi.interview.exception.NoSuchAbsenceException {

		return getPersistence().findBystatus_Last(
			groupId, status, orderByComparator);
	}

	/**
	 * Returns the last absence in the ordered set where groupId = &#63; and status = &#63;.
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching absence, or <code>null</code> if a matching absence could not be found
	 */
	public static Absence fetchBystatus_Last(
		long groupId, int status,
		OrderByComparator<Absence> orderByComparator) {

		return getPersistence().fetchBystatus_Last(
			groupId, status, orderByComparator);
	}

	/**
	 * Returns the absences before and after the current absence in the ordered set where groupId = &#63; and status = &#63;.
	 *
	 * @param absenceId the primary key of the current absence
	 * @param groupId the group ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next absence
	 * @throws NoSuchAbsenceException if a absence with the primary key could not be found
	 */
	public static Absence[] findBystatus_PrevAndNext(
			long absenceId, long groupId, int status,
			OrderByComparator<Absence> orderByComparator)
		throws com.cgi.interview.exception.NoSuchAbsenceException {

		return getPersistence().findBystatus_PrevAndNext(
			absenceId, groupId, status, orderByComparator);
	}

	/**
	 * Removes all the absences where groupId = &#63; and status = &#63; from the database.
	 *
	 * @param groupId the group ID
	 * @param status the status
	 */
	public static void removeBystatus(long groupId, int status) {
		getPersistence().removeBystatus(groupId, status);
	}

	/**
	 * Returns the number of absences where groupId = &#63; and status = &#63;.
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @return the number of matching absences
	 */
	public static int countBystatus(long groupId, int status) {
		return getPersistence().countBystatus(groupId, status);
	}

	/**
	 * Returns all the absences where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the matching absences
	 */
	public static List<Absence> findByuserId(long userId) {
		return getPersistence().findByuserId(userId);
	}

	/**
	 * Returns a range of all the absences where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AbsenceModelImpl</code>.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of absences
	 * @param end the upper bound of the range of absences (not inclusive)
	 * @return the range of matching absences
	 */
	public static List<Absence> findByuserId(long userId, int start, int end) {
		return getPersistence().findByuserId(userId, start, end);
	}

	/**
	 * Returns an ordered range of all the absences where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AbsenceModelImpl</code>.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of absences
	 * @param end the upper bound of the range of absences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching absences
	 */
	public static List<Absence> findByuserId(
		long userId, int start, int end,
		OrderByComparator<Absence> orderByComparator) {

		return getPersistence().findByuserId(
			userId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the absences where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AbsenceModelImpl</code>.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of absences
	 * @param end the upper bound of the range of absences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching absences
	 */
	public static List<Absence> findByuserId(
		long userId, int start, int end,
		OrderByComparator<Absence> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByuserId(
			userId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first absence in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching absence
	 * @throws NoSuchAbsenceException if a matching absence could not be found
	 */
	public static Absence findByuserId_First(
			long userId, OrderByComparator<Absence> orderByComparator)
		throws com.cgi.interview.exception.NoSuchAbsenceException {

		return getPersistence().findByuserId_First(userId, orderByComparator);
	}

	/**
	 * Returns the first absence in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching absence, or <code>null</code> if a matching absence could not be found
	 */
	public static Absence fetchByuserId_First(
		long userId, OrderByComparator<Absence> orderByComparator) {

		return getPersistence().fetchByuserId_First(userId, orderByComparator);
	}

	/**
	 * Returns the last absence in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching absence
	 * @throws NoSuchAbsenceException if a matching absence could not be found
	 */
	public static Absence findByuserId_Last(
			long userId, OrderByComparator<Absence> orderByComparator)
		throws com.cgi.interview.exception.NoSuchAbsenceException {

		return getPersistence().findByuserId_Last(userId, orderByComparator);
	}

	/**
	 * Returns the last absence in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching absence, or <code>null</code> if a matching absence could not be found
	 */
	public static Absence fetchByuserId_Last(
		long userId, OrderByComparator<Absence> orderByComparator) {

		return getPersistence().fetchByuserId_Last(userId, orderByComparator);
	}

	/**
	 * Returns the absences before and after the current absence in the ordered set where userId = &#63;.
	 *
	 * @param absenceId the primary key of the current absence
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next absence
	 * @throws NoSuchAbsenceException if a absence with the primary key could not be found
	 */
	public static Absence[] findByuserId_PrevAndNext(
			long absenceId, long userId,
			OrderByComparator<Absence> orderByComparator)
		throws com.cgi.interview.exception.NoSuchAbsenceException {

		return getPersistence().findByuserId_PrevAndNext(
			absenceId, userId, orderByComparator);
	}

	/**
	 * Removes all the absences where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 */
	public static void removeByuserId(long userId) {
		getPersistence().removeByuserId(userId);
	}

	/**
	 * Returns the number of absences where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching absences
	 */
	public static int countByuserId(long userId) {
		return getPersistence().countByuserId(userId);
	}

	/**
	 * Caches the absence in the entity cache if it is enabled.
	 *
	 * @param absence the absence
	 */
	public static void cacheResult(Absence absence) {
		getPersistence().cacheResult(absence);
	}

	/**
	 * Caches the absences in the entity cache if it is enabled.
	 *
	 * @param absences the absences
	 */
	public static void cacheResult(List<Absence> absences) {
		getPersistence().cacheResult(absences);
	}

	/**
	 * Creates a new absence with the primary key. Does not add the absence to the database.
	 *
	 * @param absenceId the primary key for the new absence
	 * @return the new absence
	 */
	public static Absence create(long absenceId) {
		return getPersistence().create(absenceId);
	}

	/**
	 * Removes the absence with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param absenceId the primary key of the absence
	 * @return the absence that was removed
	 * @throws NoSuchAbsenceException if a absence with the primary key could not be found
	 */
	public static Absence remove(long absenceId)
		throws com.cgi.interview.exception.NoSuchAbsenceException {

		return getPersistence().remove(absenceId);
	}

	public static Absence updateImpl(Absence absence) {
		return getPersistence().updateImpl(absence);
	}

	/**
	 * Returns the absence with the primary key or throws a <code>NoSuchAbsenceException</code> if it could not be found.
	 *
	 * @param absenceId the primary key of the absence
	 * @return the absence
	 * @throws NoSuchAbsenceException if a absence with the primary key could not be found
	 */
	public static Absence findByPrimaryKey(long absenceId)
		throws com.cgi.interview.exception.NoSuchAbsenceException {

		return getPersistence().findByPrimaryKey(absenceId);
	}

	/**
	 * Returns the absence with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param absenceId the primary key of the absence
	 * @return the absence, or <code>null</code> if a absence with the primary key could not be found
	 */
	public static Absence fetchByPrimaryKey(long absenceId) {
		return getPersistence().fetchByPrimaryKey(absenceId);
	}

	/**
	 * Returns all the absences.
	 *
	 * @return the absences
	 */
	public static List<Absence> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the absences.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AbsenceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of absences
	 * @param end the upper bound of the range of absences (not inclusive)
	 * @return the range of absences
	 */
	public static List<Absence> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the absences.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AbsenceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of absences
	 * @param end the upper bound of the range of absences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of absences
	 */
	public static List<Absence> findAll(
		int start, int end, OrderByComparator<Absence> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the absences.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AbsenceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of absences
	 * @param end the upper bound of the range of absences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of absences
	 */
	public static List<Absence> findAll(
		int start, int end, OrderByComparator<Absence> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the absences from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of absences.
	 *
	 * @return the number of absences
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static AbsencePersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<AbsencePersistence, AbsencePersistence>
		_serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(AbsencePersistence.class);

		ServiceTracker<AbsencePersistence, AbsencePersistence> serviceTracker =
			new ServiceTracker<AbsencePersistence, AbsencePersistence>(
				bundle.getBundleContext(), AbsencePersistence.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}