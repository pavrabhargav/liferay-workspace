/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.cgi.interview.service;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for Absence. This utility wraps
 * <code>com.cgi.interview.service.impl.AbsenceLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see AbsenceLocalService
 * @generated
 */
public class AbsenceLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>com.cgi.interview.service.impl.AbsenceLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the absence to the database. Also notifies the appropriate model listeners.
	 *
	 * @param absence the absence
	 * @return the absence that was added
	 */
	public static com.cgi.interview.model.Absence addAbsence(
		com.cgi.interview.model.Absence absence) {

		return getService().addAbsence(absence);
	}

	public static com.cgi.interview.model.Absence addAbsence(
		com.liferay.portal.kernel.service.ServiceContext serviceContext,
		long userId, String type, java.util.Date startDate,
		java.util.Date endDate, String reason) {

		return getService().addAbsence(
			serviceContext, userId, type, startDate, endDate, reason);
	}

	/**
	 * Creates a new absence with the primary key. Does not add the absence to the database.
	 *
	 * @param absenceId the primary key for the new absence
	 * @return the new absence
	 */
	public static com.cgi.interview.model.Absence createAbsence(
		long absenceId) {

		return getService().createAbsence(absenceId);
	}

	/**
	 * Deletes the absence from the database. Also notifies the appropriate model listeners.
	 *
	 * @param absence the absence
	 * @return the absence that was removed
	 */
	public static com.cgi.interview.model.Absence deleteAbsence(
		com.cgi.interview.model.Absence absence) {

		return getService().deleteAbsence(absence);
	}

	/**
	 * Deletes the absence with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param absenceId the primary key of the absence
	 * @return the absence that was removed
	 * @throws PortalException if a absence with the primary key could not be found
	 */
	public static com.cgi.interview.model.Absence deleteAbsence(long absenceId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().deleteAbsence(absenceId);
	}

	/**
	 * @throws PortalException
	 */
	public static com.liferay.portal.kernel.model.PersistedModel
			deletePersistedModel(
				com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery
		dynamicQuery() {

		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.cgi.interview.model.impl.AbsenceModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.cgi.interview.model.impl.AbsenceModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static com.cgi.interview.model.Absence fetchAbsence(long absenceId) {
		return getService().fetchAbsence(absenceId);
	}

	/**
	 * Returns the absence matching the UUID and group.
	 *
	 * @param uuid the absence's UUID
	 * @param groupId the primary key of the group
	 * @return the matching absence, or <code>null</code> if a matching absence could not be found
	 */
	public static com.cgi.interview.model.Absence fetchAbsenceByUuidAndGroupId(
		String uuid, long groupId) {

		return getService().fetchAbsenceByUuidAndGroupId(uuid, groupId);
	}

	public static java.util.List<com.cgi.interview.model.Absence> findBystatus(
		long groupId, int status) {

		return getService().findBystatus(groupId, status);
	}

	public static java.util.List<com.cgi.interview.model.Absence> findBystatus(
		long groupId, int status, int start, int end) {

		return getService().findBystatus(groupId, status, start, end);
	}

	public static java.util.List<com.cgi.interview.model.Absence> findByuserId(
		long userId) {

		return getService().findByuserId(userId);
	}

	/**
	 * Returns the absence with the primary key.
	 *
	 * @param absenceId the primary key of the absence
	 * @return the absence
	 * @throws PortalException if a absence with the primary key could not be found
	 */
	public static com.cgi.interview.model.Absence getAbsence(long absenceId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getAbsence(absenceId);
	}

	/**
	 * Returns the absence matching the UUID and group.
	 *
	 * @param uuid the absence's UUID
	 * @param groupId the primary key of the group
	 * @return the matching absence
	 * @throws PortalException if a matching absence could not be found
	 */
	public static com.cgi.interview.model.Absence getAbsenceByUuidAndGroupId(
			String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getAbsenceByUuidAndGroupId(uuid, groupId);
	}

	/**
	 * Returns a range of all the absences.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.cgi.interview.model.impl.AbsenceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of absences
	 * @param end the upper bound of the range of absences (not inclusive)
	 * @return the range of absences
	 */
	public static java.util.List<com.cgi.interview.model.Absence> getAbsences(
		int start, int end) {

		return getService().getAbsences(start, end);
	}

	/**
	 * Returns all the absences matching the UUID and company.
	 *
	 * @param uuid the UUID of the absences
	 * @param companyId the primary key of the company
	 * @return the matching absences, or an empty list if no matches were found
	 */
	public static java.util.List<com.cgi.interview.model.Absence>
		getAbsencesByUuidAndCompanyId(String uuid, long companyId) {

		return getService().getAbsencesByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of absences matching the UUID and company.
	 *
	 * @param uuid the UUID of the absences
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of absences
	 * @param end the upper bound of the range of absences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching absences, or an empty list if no matches were found
	 */
	public static java.util.List<com.cgi.interview.model.Absence>
		getAbsencesByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<com.cgi.interview.model.Absence> orderByComparator) {

		return getService().getAbsencesByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of absences.
	 *
	 * @return the number of absences
	 */
	public static int getAbsencesCount() {
		return getService().getAbsencesCount();
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static com.liferay.portal.kernel.model.PersistedModel
			getPersistedModel(java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the absence in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param absence the absence
	 * @return the absence that was updated
	 */
	public static com.cgi.interview.model.Absence updateAbsence(
		com.cgi.interview.model.Absence absence) {

		return getService().updateAbsence(absence);
	}

	public static com.cgi.interview.model.Absence updateStatus(
		long userId, long absenceId, int status,
		com.liferay.portal.kernel.service.ServiceContext serviceContext) {

		return getService().updateStatus(
			userId, absenceId, status, serviceContext);
	}

	public static AbsenceLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<AbsenceLocalService, AbsenceLocalService>
		_serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(AbsenceLocalService.class);

		ServiceTracker<AbsenceLocalService, AbsenceLocalService>
			serviceTracker =
				new ServiceTracker<AbsenceLocalService, AbsenceLocalService>(
					bundle.getBundleContext(), AbsenceLocalService.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}