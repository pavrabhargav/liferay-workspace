/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.cgi.interview.service.persistence;

import com.cgi.interview.exception.NoSuchAbsenceException;
import com.cgi.interview.model.Absence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the absence service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AbsenceUtil
 * @generated
 */
@ProviderType
public interface AbsencePersistence extends BasePersistence<Absence> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link AbsenceUtil} to access the absence persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the absences where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching absences
	 */
	public java.util.List<Absence> findByUuid(String uuid);

	/**
	 * Returns a range of all the absences where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AbsenceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of absences
	 * @param end the upper bound of the range of absences (not inclusive)
	 * @return the range of matching absences
	 */
	public java.util.List<Absence> findByUuid(String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the absences where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AbsenceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of absences
	 * @param end the upper bound of the range of absences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching absences
	 */
	public java.util.List<Absence> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Absence>
			orderByComparator);

	/**
	 * Returns an ordered range of all the absences where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AbsenceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of absences
	 * @param end the upper bound of the range of absences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching absences
	 */
	public java.util.List<Absence> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Absence>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first absence in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching absence
	 * @throws NoSuchAbsenceException if a matching absence could not be found
	 */
	public Absence findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Absence>
				orderByComparator)
		throws NoSuchAbsenceException;

	/**
	 * Returns the first absence in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching absence, or <code>null</code> if a matching absence could not be found
	 */
	public Absence fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Absence>
			orderByComparator);

	/**
	 * Returns the last absence in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching absence
	 * @throws NoSuchAbsenceException if a matching absence could not be found
	 */
	public Absence findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Absence>
				orderByComparator)
		throws NoSuchAbsenceException;

	/**
	 * Returns the last absence in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching absence, or <code>null</code> if a matching absence could not be found
	 */
	public Absence fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Absence>
			orderByComparator);

	/**
	 * Returns the absences before and after the current absence in the ordered set where uuid = &#63;.
	 *
	 * @param absenceId the primary key of the current absence
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next absence
	 * @throws NoSuchAbsenceException if a absence with the primary key could not be found
	 */
	public Absence[] findByUuid_PrevAndNext(
			long absenceId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Absence>
				orderByComparator)
		throws NoSuchAbsenceException;

	/**
	 * Removes all the absences where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of absences where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching absences
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the absence where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchAbsenceException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching absence
	 * @throws NoSuchAbsenceException if a matching absence could not be found
	 */
	public Absence findByUUID_G(String uuid, long groupId)
		throws NoSuchAbsenceException;

	/**
	 * Returns the absence where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching absence, or <code>null</code> if a matching absence could not be found
	 */
	public Absence fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the absence where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching absence, or <code>null</code> if a matching absence could not be found
	 */
	public Absence fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache);

	/**
	 * Removes the absence where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the absence that was removed
	 */
	public Absence removeByUUID_G(String uuid, long groupId)
		throws NoSuchAbsenceException;

	/**
	 * Returns the number of absences where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching absences
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the absences where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching absences
	 */
	public java.util.List<Absence> findByUuid_C(String uuid, long companyId);

	/**
	 * Returns a range of all the absences where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AbsenceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of absences
	 * @param end the upper bound of the range of absences (not inclusive)
	 * @return the range of matching absences
	 */
	public java.util.List<Absence> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the absences where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AbsenceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of absences
	 * @param end the upper bound of the range of absences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching absences
	 */
	public java.util.List<Absence> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Absence>
			orderByComparator);

	/**
	 * Returns an ordered range of all the absences where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AbsenceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of absences
	 * @param end the upper bound of the range of absences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching absences
	 */
	public java.util.List<Absence> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Absence>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first absence in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching absence
	 * @throws NoSuchAbsenceException if a matching absence could not be found
	 */
	public Absence findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Absence>
				orderByComparator)
		throws NoSuchAbsenceException;

	/**
	 * Returns the first absence in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching absence, or <code>null</code> if a matching absence could not be found
	 */
	public Absence fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Absence>
			orderByComparator);

	/**
	 * Returns the last absence in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching absence
	 * @throws NoSuchAbsenceException if a matching absence could not be found
	 */
	public Absence findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Absence>
				orderByComparator)
		throws NoSuchAbsenceException;

	/**
	 * Returns the last absence in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching absence, or <code>null</code> if a matching absence could not be found
	 */
	public Absence fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Absence>
			orderByComparator);

	/**
	 * Returns the absences before and after the current absence in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param absenceId the primary key of the current absence
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next absence
	 * @throws NoSuchAbsenceException if a absence with the primary key could not be found
	 */
	public Absence[] findByUuid_C_PrevAndNext(
			long absenceId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Absence>
				orderByComparator)
		throws NoSuchAbsenceException;

	/**
	 * Removes all the absences where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of absences where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching absences
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the absences where groupId = &#63; and status = &#63;.
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @return the matching absences
	 */
	public java.util.List<Absence> findBystatus(long groupId, int status);

	/**
	 * Returns a range of all the absences where groupId = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AbsenceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @param start the lower bound of the range of absences
	 * @param end the upper bound of the range of absences (not inclusive)
	 * @return the range of matching absences
	 */
	public java.util.List<Absence> findBystatus(
		long groupId, int status, int start, int end);

	/**
	 * Returns an ordered range of all the absences where groupId = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AbsenceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @param start the lower bound of the range of absences
	 * @param end the upper bound of the range of absences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching absences
	 */
	public java.util.List<Absence> findBystatus(
		long groupId, int status, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Absence>
			orderByComparator);

	/**
	 * Returns an ordered range of all the absences where groupId = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AbsenceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @param start the lower bound of the range of absences
	 * @param end the upper bound of the range of absences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching absences
	 */
	public java.util.List<Absence> findBystatus(
		long groupId, int status, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Absence>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first absence in the ordered set where groupId = &#63; and status = &#63;.
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching absence
	 * @throws NoSuchAbsenceException if a matching absence could not be found
	 */
	public Absence findBystatus_First(
			long groupId, int status,
			com.liferay.portal.kernel.util.OrderByComparator<Absence>
				orderByComparator)
		throws NoSuchAbsenceException;

	/**
	 * Returns the first absence in the ordered set where groupId = &#63; and status = &#63;.
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching absence, or <code>null</code> if a matching absence could not be found
	 */
	public Absence fetchBystatus_First(
		long groupId, int status,
		com.liferay.portal.kernel.util.OrderByComparator<Absence>
			orderByComparator);

	/**
	 * Returns the last absence in the ordered set where groupId = &#63; and status = &#63;.
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching absence
	 * @throws NoSuchAbsenceException if a matching absence could not be found
	 */
	public Absence findBystatus_Last(
			long groupId, int status,
			com.liferay.portal.kernel.util.OrderByComparator<Absence>
				orderByComparator)
		throws NoSuchAbsenceException;

	/**
	 * Returns the last absence in the ordered set where groupId = &#63; and status = &#63;.
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching absence, or <code>null</code> if a matching absence could not be found
	 */
	public Absence fetchBystatus_Last(
		long groupId, int status,
		com.liferay.portal.kernel.util.OrderByComparator<Absence>
			orderByComparator);

	/**
	 * Returns the absences before and after the current absence in the ordered set where groupId = &#63; and status = &#63;.
	 *
	 * @param absenceId the primary key of the current absence
	 * @param groupId the group ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next absence
	 * @throws NoSuchAbsenceException if a absence with the primary key could not be found
	 */
	public Absence[] findBystatus_PrevAndNext(
			long absenceId, long groupId, int status,
			com.liferay.portal.kernel.util.OrderByComparator<Absence>
				orderByComparator)
		throws NoSuchAbsenceException;

	/**
	 * Removes all the absences where groupId = &#63; and status = &#63; from the database.
	 *
	 * @param groupId the group ID
	 * @param status the status
	 */
	public void removeBystatus(long groupId, int status);

	/**
	 * Returns the number of absences where groupId = &#63; and status = &#63;.
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @return the number of matching absences
	 */
	public int countBystatus(long groupId, int status);

	/**
	 * Returns all the absences where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the matching absences
	 */
	public java.util.List<Absence> findByuserId(long userId);

	/**
	 * Returns a range of all the absences where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AbsenceModelImpl</code>.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of absences
	 * @param end the upper bound of the range of absences (not inclusive)
	 * @return the range of matching absences
	 */
	public java.util.List<Absence> findByuserId(
		long userId, int start, int end);

	/**
	 * Returns an ordered range of all the absences where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AbsenceModelImpl</code>.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of absences
	 * @param end the upper bound of the range of absences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching absences
	 */
	public java.util.List<Absence> findByuserId(
		long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Absence>
			orderByComparator);

	/**
	 * Returns an ordered range of all the absences where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AbsenceModelImpl</code>.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of absences
	 * @param end the upper bound of the range of absences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching absences
	 */
	public java.util.List<Absence> findByuserId(
		long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Absence>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first absence in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching absence
	 * @throws NoSuchAbsenceException if a matching absence could not be found
	 */
	public Absence findByuserId_First(
			long userId,
			com.liferay.portal.kernel.util.OrderByComparator<Absence>
				orderByComparator)
		throws NoSuchAbsenceException;

	/**
	 * Returns the first absence in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching absence, or <code>null</code> if a matching absence could not be found
	 */
	public Absence fetchByuserId_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator<Absence>
			orderByComparator);

	/**
	 * Returns the last absence in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching absence
	 * @throws NoSuchAbsenceException if a matching absence could not be found
	 */
	public Absence findByuserId_Last(
			long userId,
			com.liferay.portal.kernel.util.OrderByComparator<Absence>
				orderByComparator)
		throws NoSuchAbsenceException;

	/**
	 * Returns the last absence in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching absence, or <code>null</code> if a matching absence could not be found
	 */
	public Absence fetchByuserId_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator<Absence>
			orderByComparator);

	/**
	 * Returns the absences before and after the current absence in the ordered set where userId = &#63;.
	 *
	 * @param absenceId the primary key of the current absence
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next absence
	 * @throws NoSuchAbsenceException if a absence with the primary key could not be found
	 */
	public Absence[] findByuserId_PrevAndNext(
			long absenceId, long userId,
			com.liferay.portal.kernel.util.OrderByComparator<Absence>
				orderByComparator)
		throws NoSuchAbsenceException;

	/**
	 * Removes all the absences where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 */
	public void removeByuserId(long userId);

	/**
	 * Returns the number of absences where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching absences
	 */
	public int countByuserId(long userId);

	/**
	 * Caches the absence in the entity cache if it is enabled.
	 *
	 * @param absence the absence
	 */
	public void cacheResult(Absence absence);

	/**
	 * Caches the absences in the entity cache if it is enabled.
	 *
	 * @param absences the absences
	 */
	public void cacheResult(java.util.List<Absence> absences);

	/**
	 * Creates a new absence with the primary key. Does not add the absence to the database.
	 *
	 * @param absenceId the primary key for the new absence
	 * @return the new absence
	 */
	public Absence create(long absenceId);

	/**
	 * Removes the absence with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param absenceId the primary key of the absence
	 * @return the absence that was removed
	 * @throws NoSuchAbsenceException if a absence with the primary key could not be found
	 */
	public Absence remove(long absenceId) throws NoSuchAbsenceException;

	public Absence updateImpl(Absence absence);

	/**
	 * Returns the absence with the primary key or throws a <code>NoSuchAbsenceException</code> if it could not be found.
	 *
	 * @param absenceId the primary key of the absence
	 * @return the absence
	 * @throws NoSuchAbsenceException if a absence with the primary key could not be found
	 */
	public Absence findByPrimaryKey(long absenceId)
		throws NoSuchAbsenceException;

	/**
	 * Returns the absence with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param absenceId the primary key of the absence
	 * @return the absence, or <code>null</code> if a absence with the primary key could not be found
	 */
	public Absence fetchByPrimaryKey(long absenceId);

	/**
	 * Returns all the absences.
	 *
	 * @return the absences
	 */
	public java.util.List<Absence> findAll();

	/**
	 * Returns a range of all the absences.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AbsenceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of absences
	 * @param end the upper bound of the range of absences (not inclusive)
	 * @return the range of absences
	 */
	public java.util.List<Absence> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the absences.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AbsenceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of absences
	 * @param end the upper bound of the range of absences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of absences
	 */
	public java.util.List<Absence> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Absence>
			orderByComparator);

	/**
	 * Returns an ordered range of all the absences.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AbsenceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of absences
	 * @param end the upper bound of the range of absences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of absences
	 */
	public java.util.List<Absence> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Absence>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the absences from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of absences.
	 *
	 * @return the number of absences
	 */
	public int countAll();

}