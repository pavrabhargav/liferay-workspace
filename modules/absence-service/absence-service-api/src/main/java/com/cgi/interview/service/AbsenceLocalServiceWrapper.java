/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.cgi.interview.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link AbsenceLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see AbsenceLocalService
 * @generated
 */
public class AbsenceLocalServiceWrapper
	implements AbsenceLocalService, ServiceWrapper<AbsenceLocalService> {

	public AbsenceLocalServiceWrapper(AbsenceLocalService absenceLocalService) {
		_absenceLocalService = absenceLocalService;
	}

	/**
	 * Adds the absence to the database. Also notifies the appropriate model listeners.
	 *
	 * @param absence the absence
	 * @return the absence that was added
	 */
	@Override
	public com.cgi.interview.model.Absence addAbsence(
		com.cgi.interview.model.Absence absence) {

		return _absenceLocalService.addAbsence(absence);
	}

	@Override
	public com.cgi.interview.model.Absence addAbsence(
		com.liferay.portal.kernel.service.ServiceContext serviceContext,
		long userId, String type, java.util.Date startDate,
		java.util.Date endDate, String reason) {

		return _absenceLocalService.addAbsence(
			serviceContext, userId, type, startDate, endDate, reason);
	}

	/**
	 * Creates a new absence with the primary key. Does not add the absence to the database.
	 *
	 * @param absenceId the primary key for the new absence
	 * @return the new absence
	 */
	@Override
	public com.cgi.interview.model.Absence createAbsence(long absenceId) {
		return _absenceLocalService.createAbsence(absenceId);
	}

	/**
	 * Deletes the absence from the database. Also notifies the appropriate model listeners.
	 *
	 * @param absence the absence
	 * @return the absence that was removed
	 */
	@Override
	public com.cgi.interview.model.Absence deleteAbsence(
		com.cgi.interview.model.Absence absence) {

		return _absenceLocalService.deleteAbsence(absence);
	}

	/**
	 * Deletes the absence with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param absenceId the primary key of the absence
	 * @return the absence that was removed
	 * @throws PortalException if a absence with the primary key could not be found
	 */
	@Override
	public com.cgi.interview.model.Absence deleteAbsence(long absenceId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _absenceLocalService.deleteAbsence(absenceId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _absenceLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _absenceLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _absenceLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.cgi.interview.model.impl.AbsenceModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _absenceLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.cgi.interview.model.impl.AbsenceModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _absenceLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _absenceLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _absenceLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public com.cgi.interview.model.Absence fetchAbsence(long absenceId) {
		return _absenceLocalService.fetchAbsence(absenceId);
	}

	/**
	 * Returns the absence matching the UUID and group.
	 *
	 * @param uuid the absence's UUID
	 * @param groupId the primary key of the group
	 * @return the matching absence, or <code>null</code> if a matching absence could not be found
	 */
	@Override
	public com.cgi.interview.model.Absence fetchAbsenceByUuidAndGroupId(
		String uuid, long groupId) {

		return _absenceLocalService.fetchAbsenceByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public java.util.List<com.cgi.interview.model.Absence> findBystatus(
		long groupId, int status) {

		return _absenceLocalService.findBystatus(groupId, status);
	}

	@Override
	public java.util.List<com.cgi.interview.model.Absence> findBystatus(
		long groupId, int status, int start, int end) {

		return _absenceLocalService.findBystatus(groupId, status, start, end);
	}

	@Override
	public java.util.List<com.cgi.interview.model.Absence> findByuserId(
		long userId) {

		return _absenceLocalService.findByuserId(userId);
	}

	/**
	 * Returns the absence with the primary key.
	 *
	 * @param absenceId the primary key of the absence
	 * @return the absence
	 * @throws PortalException if a absence with the primary key could not be found
	 */
	@Override
	public com.cgi.interview.model.Absence getAbsence(long absenceId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _absenceLocalService.getAbsence(absenceId);
	}

	/**
	 * Returns the absence matching the UUID and group.
	 *
	 * @param uuid the absence's UUID
	 * @param groupId the primary key of the group
	 * @return the matching absence
	 * @throws PortalException if a matching absence could not be found
	 */
	@Override
	public com.cgi.interview.model.Absence getAbsenceByUuidAndGroupId(
			String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _absenceLocalService.getAbsenceByUuidAndGroupId(uuid, groupId);
	}

	/**
	 * Returns a range of all the absences.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.cgi.interview.model.impl.AbsenceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of absences
	 * @param end the upper bound of the range of absences (not inclusive)
	 * @return the range of absences
	 */
	@Override
	public java.util.List<com.cgi.interview.model.Absence> getAbsences(
		int start, int end) {

		return _absenceLocalService.getAbsences(start, end);
	}

	/**
	 * Returns all the absences matching the UUID and company.
	 *
	 * @param uuid the UUID of the absences
	 * @param companyId the primary key of the company
	 * @return the matching absences, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<com.cgi.interview.model.Absence>
		getAbsencesByUuidAndCompanyId(String uuid, long companyId) {

		return _absenceLocalService.getAbsencesByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of absences matching the UUID and company.
	 *
	 * @param uuid the UUID of the absences
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of absences
	 * @param end the upper bound of the range of absences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching absences, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<com.cgi.interview.model.Absence>
		getAbsencesByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<com.cgi.interview.model.Absence> orderByComparator) {

		return _absenceLocalService.getAbsencesByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of absences.
	 *
	 * @return the number of absences
	 */
	@Override
	public int getAbsencesCount() {
		return _absenceLocalService.getAbsencesCount();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _absenceLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _absenceLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _absenceLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _absenceLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _absenceLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the absence in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param absence the absence
	 * @return the absence that was updated
	 */
	@Override
	public com.cgi.interview.model.Absence updateAbsence(
		com.cgi.interview.model.Absence absence) {

		return _absenceLocalService.updateAbsence(absence);
	}

	@Override
	public com.cgi.interview.model.Absence updateStatus(
		long userId, long absenceId, int status,
		com.liferay.portal.kernel.service.ServiceContext serviceContext) {

		return _absenceLocalService.updateStatus(
			userId, absenceId, status, serviceContext);
	}

	@Override
	public AbsenceLocalService getWrappedService() {
		return _absenceLocalService;
	}

	@Override
	public void setWrappedService(AbsenceLocalService absenceLocalService) {
		_absenceLocalService = absenceLocalService;
	}

	private AbsenceLocalService _absenceLocalService;

}