/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.cgi.interview.service.impl;

import com.cgi.interview.model.Absence;
import com.cgi.interview.service.base.AbsenceLocalServiceBaseImpl;
import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.Indexer;
import com.liferay.portal.kernel.search.IndexerRegistryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.ContentTypes;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.kernel.workflow.WorkflowHandlerRegistryUtil;

import java.util.Date;
import java.util.List;

import org.osgi.service.component.annotations.Component;

/**
 * The implementation of the absence local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>com.cgi.interview.service.AbsenceLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AbsenceLocalServiceBaseImpl
 */
@Component(
	property = "model.class.name=com.cgi.interview.model.Absence",
	service = AopService.class
)
public class AbsenceLocalServiceImpl extends AbsenceLocalServiceBaseImpl {
	
	public Absence addAbsence(ServiceContext serviceContext, long userId, String type, Date startDate, Date endDate, String reason ){
		long absenceId = counterLocalService.increment();
		Absence absence = null;
		
		try {
			absence = absenceLocalService.createAbsence(absenceId);

			User user = userLocalService.getUser(userId);

			absence.setUserId(userId);
			absence.setType(type);
			absence.setStartDate(startDate);
			absence.setEndDate(endDate);
			absence.setReason(reason);
			
			absence.setStatus(WorkflowConstants.STATUS_DRAFT);
			absence.setStatusByUserId(user.getUserId());
			absence.setStatusDate(new Date());
			absence.setStatusByUserName(user.getFullName());
			absence.setStatusByUserUuid(user.getUserUuid());
			
			absence = absenceLocalService.addAbsence(absence);
			
			AssetEntry assetEntry = assetEntryLocalService.updateEntry(user.getUserId(), serviceContext.getScopeGroupId(), new Date(),
		            new Date(), Absence.class.getName(), absenceId, absence.getUuid(), 0, null, null, true, false, new Date(), null,
		            new Date(), null, ContentTypes.TEXT_HTML, absence.getType(), absence.getReason(), null, null, null, 0, 0, null);

		    Indexer<Absence> indexer = IndexerRegistryUtil.nullSafeGetIndexer(Absence.class);

		    indexer.reindex(absence);
		 
		    WorkflowHandlerRegistryUtil.startWorkflowInstance(absence.getCompanyId(), absence.getGroupId(), absence.getUserId(), Absence.class.getName(),
		    		   absence.getPrimaryKey(), absence, serviceContext);
		} catch (PortalException e) {
			e.printStackTrace();
		}

		return absence;
	}
	
	
	public Absence updateStatus(long userId, long absenceId, int status,ServiceContext serviceContext){
		 
		Absence absence = absencePersistence.fetchByPrimaryKey(absenceId);
		absence.setStatus(status);
		absence.setStatusByUserId(userId);
		absence.setStatusDate(new Date());
		
		User user = null;
		 
		 try {
		      user = userLocalService.getUser(userId);
		      absence.setStatusByUserName(user.getFullName());
		      absence.setStatusByUserUuid(user.getUserUuid());
		      
		 } catch (PortalException e) {
		     e.printStackTrace();
		 }
		 
		 absence = absencePersistence.update(absence);

		 try {
			 if (status == WorkflowConstants.STATUS_APPROVED) {  
			     // update the asset status to visibile
			    assetEntryLocalService.updateEntry(Absence.class.getName(), absenceId, new Date(),null, true, true);
			 } else {
			     // set leave entity status to false
			     assetEntryLocalService.updateVisible(Absence.class.getName(), absenceId, false);  
			 }
		 } catch (Exception e) {
		     e.printStackTrace();
		 }
		 
		 return absence;
	}
	

	public List<Absence> findBystatus(long groupId, int status) {
		return absencePersistence.findBystatus(groupId, status);
	}
	
	public List<Absence> findBystatus(long groupId, int status, int start, int end) {
		return absencePersistence.findBystatus(groupId, status, start, end);
	}
	
	public List<Absence> findByuserId(long userId) {
		return absencePersistence.findByuserId(userId);
	}
}