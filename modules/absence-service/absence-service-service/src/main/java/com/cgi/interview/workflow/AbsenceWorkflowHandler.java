package com.cgi.interview.workflow;

import com.cgi.interview.model.Absence;
import com.cgi.interview.service.AbsenceLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.security.permission.ResourceActionsUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.workflow.BaseWorkflowHandler;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.kernel.workflow.WorkflowHandler;

import java.io.Serializable;
import java.util.Locale;
import java.util.Map;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, service = WorkflowHandler.class)
public class AbsenceWorkflowHandler extends BaseWorkflowHandler<Absence>  {

	@Override
	public String getClassName() {
		// TODO Auto-generated method stub
		return Absence.class.getName();
	}

	@Override
	public String getType(Locale locale) {
		 return ResourceActionsUtil.getModelResource(locale, getClassName());
	}

	@Override
	public Absence updateStatus(int status, Map<String, Serializable> workflowContext) throws PortalException {
		long userId = GetterUtil.getLong(
	            (String)workflowContext.get(WorkflowConstants.CONTEXT_USER_ID));
	        long resourcePrimKey = GetterUtil.getLong(
	            (String)workflowContext.get(
	                WorkflowConstants.CONTEXT_ENTRY_CLASS_PK));

	        ServiceContext serviceContext = (ServiceContext)workflowContext.get(
	            "serviceContext");

	        return _absenceLocalService.updateStatus(
	            userId, resourcePrimKey, status, serviceContext);
	}
	

    @Reference
    private AbsenceLocalService _absenceLocalService;

}
