/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.cgi.interview.model.impl;

import com.cgi.interview.model.Absence;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Absence in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class AbsenceCacheModel implements CacheModel<Absence>, Externalizable {

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof AbsenceCacheModel)) {
			return false;
		}

		AbsenceCacheModel absenceCacheModel = (AbsenceCacheModel)obj;

		if (absenceId == absenceCacheModel.absenceId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, absenceId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(33);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", absenceId=");
		sb.append(absenceId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", status=");
		sb.append(status);
		sb.append(", statusByUserId=");
		sb.append(statusByUserId);
		sb.append(", statusByUserName=");
		sb.append(statusByUserName);
		sb.append(", statusDate=");
		sb.append(statusDate);
		sb.append(", type=");
		sb.append(type);
		sb.append(", startDate=");
		sb.append(startDate);
		sb.append(", endDate=");
		sb.append(endDate);
		sb.append(", reason=");
		sb.append(reason);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Absence toEntityModel() {
		AbsenceImpl absenceImpl = new AbsenceImpl();

		if (uuid == null) {
			absenceImpl.setUuid("");
		}
		else {
			absenceImpl.setUuid(uuid);
		}

		absenceImpl.setAbsenceId(absenceId);
		absenceImpl.setGroupId(groupId);
		absenceImpl.setCompanyId(companyId);
		absenceImpl.setUserId(userId);

		if (userName == null) {
			absenceImpl.setUserName("");
		}
		else {
			absenceImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			absenceImpl.setCreateDate(null);
		}
		else {
			absenceImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			absenceImpl.setModifiedDate(null);
		}
		else {
			absenceImpl.setModifiedDate(new Date(modifiedDate));
		}

		absenceImpl.setStatus(status);
		absenceImpl.setStatusByUserId(statusByUserId);

		if (statusByUserName == null) {
			absenceImpl.setStatusByUserName("");
		}
		else {
			absenceImpl.setStatusByUserName(statusByUserName);
		}

		if (statusDate == Long.MIN_VALUE) {
			absenceImpl.setStatusDate(null);
		}
		else {
			absenceImpl.setStatusDate(new Date(statusDate));
		}

		if (type == null) {
			absenceImpl.setType("");
		}
		else {
			absenceImpl.setType(type);
		}

		if (startDate == Long.MIN_VALUE) {
			absenceImpl.setStartDate(null);
		}
		else {
			absenceImpl.setStartDate(new Date(startDate));
		}

		if (endDate == Long.MIN_VALUE) {
			absenceImpl.setEndDate(null);
		}
		else {
			absenceImpl.setEndDate(new Date(endDate));
		}

		if (reason == null) {
			absenceImpl.setReason("");
		}
		else {
			absenceImpl.setReason(reason);
		}

		absenceImpl.resetOriginalValues();

		return absenceImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		absenceId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		status = objectInput.readInt();

		statusByUserId = objectInput.readLong();
		statusByUserName = objectInput.readUTF();
		statusDate = objectInput.readLong();
		type = objectInput.readUTF();
		startDate = objectInput.readLong();
		endDate = objectInput.readLong();
		reason = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(absenceId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeInt(status);

		objectOutput.writeLong(statusByUserId);

		if (statusByUserName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(statusByUserName);
		}

		objectOutput.writeLong(statusDate);

		if (type == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(type);
		}

		objectOutput.writeLong(startDate);
		objectOutput.writeLong(endDate);

		if (reason == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(reason);
		}
	}

	public String uuid;
	public long absenceId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public int status;
	public long statusByUserId;
	public String statusByUserName;
	public long statusDate;
	public String type;
	public long startDate;
	public long endDate;
	public String reason;

}