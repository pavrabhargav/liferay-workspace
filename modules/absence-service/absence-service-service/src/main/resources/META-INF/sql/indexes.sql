create index IX_2CD88886 on FOO_Absence (groupId, status);
create index IX_7DB76C84 on FOO_Absence (userId);
create index IX_C0741BE on FOO_Absence (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_55511FC0 on FOO_Absence (uuid_[$COLUMN_LENGTH:75$], groupId);