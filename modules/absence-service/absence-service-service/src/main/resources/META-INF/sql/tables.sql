create table FOO_Absence (
	uuid_ VARCHAR(75) null,
	absenceId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	status INTEGER,
	statusByUserId LONG,
	statusByUserName VARCHAR(75) null,
	statusDate DATE null,
	type_ VARCHAR(75) null,
	startDate DATE null,
	endDate DATE null,
	reason VARCHAR(75) null
);