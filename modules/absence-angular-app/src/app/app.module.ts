import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { AbsenceService } from './services/absence.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { AbsenceListComponent } from './absence-list/absence-list.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {MatExpansionModule} from '@angular/material/expansion';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import { MatDatepickerModule, MatNativeDateModule } from '@angular/material';
import { MatInputModule } from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';



@NgModule({
	imports: [BrowserModule,  BrowserAnimationsModule, FormsModule, ReactiveFormsModule, HttpClientModule,
		MatExpansionModule, MatDatepickerModule,MatFormFieldModule, MatSelectModule,
		 MatNativeDateModule, MatInputModule, MatButtonModule, MatIconModule],
	declarations: [AppComponent, AbsenceListComponent],
	entryComponents: [AppComponent],
	bootstrap: [], // Don't bootstrap any component statically (see ngDoBootstrap() below)
	providers: [AbsenceService, MatDatepickerModule, MatNativeDateModule],
})
export class AppModule {
	// Avoid bootstraping any component statically because we need to attach to
	// the portlet's DOM, which is different for each portlet instance and,
	// thus, cannot be determined until the page is rendered (during runtime).
	ngDoBootstrap() {}
}
