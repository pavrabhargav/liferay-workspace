import { Component, EventEmitter, OnInit, Output } from '@angular/core';

import {AbsenceService} from '../services/absence.service';

import LiferayParams from '../../types/LiferayParams'

import {UserAbsence} from '../model/userAbsence';
import { Absence } from '../model/absence';

declare const Liferay: any;

@Component({
	templateUrl: 
		Liferay.ThemeDisplay.getPathContext() + 
		'/o/absence-angular-app/app/absence-list/absence-list.component.html',
	selector: 'absence-list'
})
export class AbsenceListComponent implements OnInit {
	params: LiferayParams;
	labels: any;
	userAbsence:UserAbsence;
	absence:Absence;

	@Output() messageFromChild = new EventEmitter();


	constructor(private absenceService:AbsenceService) {
	}

	ngOnInit() {
		this.absenceService.getUserAbsenceList().subscribe(data => this.userAbsence = data);
	}
	
	editLeave(absenceId:number) {
		//this.absenceService.getAbsenceById(absenceId).subscribe(data => );

		this.absence = this.userAbsence.userAbsence.find(e => e.absenceId == absenceId);

		//update logic
		this.messageFromChild.emit(this.absence);

	}

	deleteLeave(absenceId:number) {
		this.absenceService.deleteLeave(absenceId).subscribe(data => alert("Leave deleted"));
	}
}
