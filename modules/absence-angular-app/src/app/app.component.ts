import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import {AbsenceService} from './services/absence.service';
import {Absence} from './model/absence';

import LiferayParams from '../types/LiferayParams'

declare const Liferay: any;

@Component({
	templateUrl: 
		Liferay.ThemeDisplay.getPathContext() + 
		'/o/absence-angular-app/app/app.component.html'
})
export class AppComponent implements OnInit {
	absenceForm: FormGroup;
	submitted: boolean = false;
	employeeName:string = "";
	startMinDate = new Date();
	startMaxDate = new Date(8640000000000000);
	endMinDate = new Date();
	totalDays: number = 0;

	types: LeaveType[] = [
		{value: 'AA0', viewValue: 'Sick Leave'},
		{value: 'AA1', viewValue: 'Emergency Leave'}
	];
	
	constructor(private formBuilder: FormBuilder, private absenceService:AbsenceService) {
	}

	ngOnInit() {

		this.absenceService.getUsername().subscribe(data => {
			this.employeeName = data
		});

        this.absenceForm = this.formBuilder.group({
			userId: [Liferay.ThemeDisplay.getUserId()],
			absenceId: [''],
			employeeName: [this.employeeName],
            type: ['', Validators.required],
            startDate: ['', Validators.required],
			endDate: ['', Validators.required],
			reason: ['', Validators.required]
        });
	}

	dateChanged(startDateValue:string, endDateValue:string) {

		let startDate = new Date(startDateValue);
		let endDate = new Date(endDateValue);

		this.endMinDate = startDate;
		this.startMaxDate = endDate;

		if (startDateValue != "" && endDateValue!="") {
			let Difference_In_Time = endDate.getTime() - startDate.getTime(); 
			this.totalDays = (Difference_In_Time / (1000 * 3600 * 24)) + 1; 
		} else {
			this.totalDays = 0;
		}
	}
	
	onSubmit() {

		alert("called");
		this.submitted = true;
		console.log(this.absenceForm);

        // stop here if form is invalid
        if (this.absenceForm.invalid) {
            return;
		}
		
		let absence:Absence =  this.absenceForm.value;
		this.absenceService.addLeave(absence).subscribe();

        alert('SUCCESS!! :-)')
	}
	
	onMessageRecieved(absecne: Absence) {
		this.absenceForm.controls['absenceId'].setValue(absecne.absenceId);
		this.absenceForm.controls['type'].setValue(absecne.type);
		this.absenceForm.controls['startDate'].setValue(new Date(new Date(absecne.startDate).getTime()));
		this.absenceForm.controls['endDate'].setValue(new Date(new Date(absecne.endDate).getTime()));
		this.absenceForm.controls['reason'].setValue(absecne.reason);
	}
}

export interface LeaveType {
	value: string;
	viewValue: string;
}
