import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import {UserAbsence} from '../model/userAbsence';

import {Observable, throwError } from 'rxjs';
import { map } from 'rxjs/operators';
import { catchError } from 'rxjs/internal/operators';

import {Absence} from '../model/absence'

declare const Liferay: any;
const baseURL = "http://localhost:8080/o/absence/";

@Injectable({
    providedIn: 'root',
  })
export class AbsenceService {

   loggedInUserId = Liferay.ThemeDisplay.getUserId();

   constructor(private http : HttpClient) { }

   getAbsenceById(absenceId:number):Observable<any> {
    console.log("<<<<<<<<<<<<<<<<getAbsenceById");
        return this.http.get<Absence>(baseURL + absenceId + "?p_auth=" + Liferay.authToken)
          .pipe(catchError(this.handleError));
    }

    getUserAbsenceList():Observable<any> {
      console.log(">>>>>>>>>>>>>>>>getUserAbsenceList");
        return this.http.get<UserAbsence>(baseURL + "user/" + this.loggedInUserId + "?p_auth=" + Liferay.authToken)
          .pipe(catchError(this.handleError));
    }

   getUsername():Observable<any> {
      return this.http.get<string>(baseURL + "username/" + this.loggedInUserId + "?p_auth=" + Liferay.authToken,{responseType: 'text' as 'json'})
        .pipe(catchError(this.handleError));
   }

  addLeave(absence:Absence) {
      return this.http.post(baseURL + "?p_auth=" + Liferay.authToken, absence).pipe(catchError(this.handleError));
  }

  updateLeave(absence:Absence) {
      return this.http.put(baseURL + "?p_auth=" + Liferay.authToken, absence).pipe(catchError(this.handleError));
  }

  deleteLeave(absenceId:number) {
      return this.http.delete(baseURL + absenceId +"?p_auth=" + Liferay.authToken).pipe(catchError(this.handleError));
  }

   private handleError(error: HttpErrorResponse): any {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError(
      'Something bad happened; please try again later.');
  }

}
