export interface Absence {
    absenceId:number;
    type: string;
    reason?: string;
    startDate?: Date;
    endDate?: Date;
    userId: number;
}